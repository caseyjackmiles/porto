﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MouseController : MonoBehaviour {
	
	public GameObject mouseControlledObject;
	public GameObject mouseHoveredPosition;
	List<GameObject> positions;
	int layerMask;

	// Use this for initialization
	void Start () {
		mouseControlledObject = null;
		mouseHoveredPosition = null;
		
		positions = new List<GameObject>();
		
		Object[] positionObjs = FindObjectsOfType(typeof(PiecePosition));
		foreach (Object o in positionObjs){
		
			Component component = o as Component;
			
			positions.Add(component.gameObject);
			
		}
		Debug.Log(positions.Count + " GamePieces");
		
		layerMask = 1 << 8;
		layerMask = ~layerMask;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(mouseControlledObject){
			
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit = new RaycastHit();
			if(Physics.Raycast(ray,out hit,1000, layerMask)){
								
				bool hoveringOverPosition = false;
				foreach(GameObject p in positions){
					if((hit.collider.gameObject == p) && !(p.GetComponent<PiecePosition>().isOccupied())){
						p.SendMessage("highlightPosition");
						mouseHoveredPosition = p;
						hoveringOverPosition = true;
					}
					else {
						p.SendMessage("unhighlightPosition");
					}
				}
				
				if(!hoveringOverPosition){
					mouseHoveredPosition = null;
					
					//Unhighlight the positions if none are hovered
					foreach(GameObject p in positions){
						p.SendMessage("unhighlightPosition");
					}
				}
			}
			else {
				mouseHoveredPosition = null;
				
				foreach(GameObject p in positions){
					p.SendMessage("unhighlightPosition");
				}
			}
		}
		
	}
}
