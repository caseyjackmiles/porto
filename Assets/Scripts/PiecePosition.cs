﻿using UnityEngine;
using System.Collections;

public class PiecePosition : MonoBehaviour {
	
	public Material standardMaterial;
	public Material highlightMaterial;
	private GameObject playedPiece;
	private GameBoard gameBoard;

	
	// Use this for initialization
	void Start () {
		playedPiece = null;
		gameBoard = GameObject.Find("GameBoard").GetComponent<GameBoard>();
		if(!gameBoard)
			Debug.Log ("Could not find gameBoard object");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void highlightPosition(){
		renderer.material = highlightMaterial;
	}
	
	void unhighlightPosition(){
		renderer.material = standardMaterial;
	}
	
	public void playPiece(GameObject piece){
		
		if( ! piece.GetComponent<MouseMoveable>() ){
			Debug.Log ("Tried to playPiece that was not a GamePiece");
			return;
		}
		
		playedPiece = piece;
		
		setPieceAsChildTransform(piece);
		
		unhighlightPosition();
		
		gameBoard.checkLinesForWin();
		
	}
	
	void setPieceAsChildTransform(GameObject p){
		//Make the piece a child
		p.transform.parent = this.transform;
		
		switch((int) p.GetComponent<PieceAttributes>().pieceHeight){
		case 0:
			//pieceHeight == attr_Tall
			p.transform.localPosition = new Vector3(0F,100F,0F);
			break;
		case 1:
			//pieceHeight == attr_Short
			p.transform.localPosition = new Vector3(0F,50F,0F);
			break;
		default:
			break;
		}
	}
	
	public bool isOccupied(){
		return playedPiece;
	}
	
	/********************************
	 * CHECK PIECE ATTRIBUTES FOR WIN
	 * ******************************/
	
	public bool hasDarkPiece(){
		if(!playedPiece)
			return false;
		
		if(playedPiece.GetComponent<PieceAttributes>().pieceColor == colorAttribute.attr_Dark)
			return true;
		else
			return false;
	}
	
	public bool hasLightPiece(){
		if(!playedPiece)
			return false;
		
		if(playedPiece.GetComponent<PieceAttributes>().pieceColor == colorAttribute.attr_Light)
			return true;
		else
			return false;
	}
	
	public bool hasRoundPiece(){
		if(!playedPiece)
			return false;
		
		if(playedPiece.GetComponent<PieceAttributes>().pieceShape == shapeAttribute.attr_Round)
			return true;
		else
			return false;		
	}
	
	public bool hasSquarePiece(){
		if(!playedPiece)
			return false;
		
		if(playedPiece.GetComponent<PieceAttributes>().pieceShape == shapeAttribute.attr_Square)
			return true;
		else
			return false;		
	}
	
	public bool hasHollowPiece(){
		if(!playedPiece)
			return false;
		
		if(playedPiece.GetComponent<PieceAttributes>().pieceSolid == solidAttribute.attr_Hollow)
			return true;
		else
			return false;
	}
	
	public bool hasSolidPiece(){
		if(!playedPiece)
			return false;
		
		if(playedPiece.GetComponent<PieceAttributes>().pieceSolid == solidAttribute.attr_Solid)
			return true;
		else
			return false;		
	}
	
	public bool hasShortPiece(){
		if(!playedPiece)
			return false;
		
		if(playedPiece.GetComponent<PieceAttributes>().pieceHeight == heightAttribute.attr_Short)
			return true;
		else
			return false;		
	}
	
	public bool hasTallPiece(){
		if(!playedPiece)
			return false;
		
		if(playedPiece.GetComponent<PieceAttributes>().pieceHeight == heightAttribute.attr_Tall)
			return true;
		else
			return false;		
	}
	
}
