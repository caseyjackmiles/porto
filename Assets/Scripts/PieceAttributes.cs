﻿using UnityEngine;
using System.Collections;

	public enum heightAttribute { attr_Tall, attr_Short };
	public enum colorAttribute  { attr_Dark, attr_Light };
	public enum solidAttribute  { attr_Solid, attr_Hollow };
	public enum shapeAttribute  { attr_Square, attr_Round };

public class PieceAttributes : MonoBehaviour {


	
	public colorAttribute  pieceColor;
	public shapeAttribute  pieceShape;
	public solidAttribute  pieceSolid;
	public heightAttribute pieceHeight;
}
