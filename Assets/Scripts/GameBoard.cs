﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameBoard : MonoBehaviour {

	
	//Declare the 'win-lines' here.
	//Should be 10 arrays, each of 4 PiecePositions
	List<List<PiecePosition>> winLines;
	List<PiecePosition> line1;
	List<PiecePosition> line2;
	List<PiecePosition> line3;
	List<PiecePosition> line4;
	List<PiecePosition> line5;
	List<PiecePosition> line6;
	List<PiecePosition> line7;
	List<PiecePosition> line8;
	List<PiecePosition> line9;
	List<PiecePosition> line10;
	
	
	// Use this for initialization
	void Start () {
		
		//Get all of the PiecePositions
		PiecePosition pos1  = GameObject.Find("PiecePosition1" ).GetComponent<PiecePosition>();
		PiecePosition pos2  = GameObject.Find("PiecePosition2" ).GetComponent<PiecePosition>();
		PiecePosition pos3  = GameObject.Find("PiecePosition3" ).GetComponent<PiecePosition>();
		PiecePosition pos4  = GameObject.Find("PiecePosition4" ).GetComponent<PiecePosition>();
		PiecePosition pos5  = GameObject.Find("PiecePosition5" ).GetComponent<PiecePosition>();
		PiecePosition pos6  = GameObject.Find("PiecePosition6" ).GetComponent<PiecePosition>();
		PiecePosition pos7  = GameObject.Find("PiecePosition7" ).GetComponent<PiecePosition>();
		PiecePosition pos8  = GameObject.Find("PiecePosition8" ).GetComponent<PiecePosition>();
		PiecePosition pos9  = GameObject.Find("PiecePosition9" ).GetComponent<PiecePosition>();
		PiecePosition pos10 = GameObject.Find("PiecePosition10").GetComponent<PiecePosition>();
		PiecePosition pos11 = GameObject.Find("PiecePosition11").GetComponent<PiecePosition>();
		PiecePosition pos12 = GameObject.Find("PiecePosition12").GetComponent<PiecePosition>();
		PiecePosition pos13 = GameObject.Find("PiecePosition13").GetComponent<PiecePosition>();
		PiecePosition pos14 = GameObject.Find("PiecePosition14").GetComponent<PiecePosition>();
		PiecePosition pos15 = GameObject.Find("PiecePosition15").GetComponent<PiecePosition>();
		PiecePosition pos16 = GameObject.Find("PiecePosition16").GetComponent<PiecePosition>();


		//Create lines using the PiecePositions
		line1  = new List<PiecePosition>(){ pos1,  pos2,  pos3,  pos4  };	// Horizontals
		line2  = new List<PiecePosition>(){ pos5,  pos6,  pos7,  pos8  };
		line3  = new List<PiecePosition>(){ pos9,  pos10, pos11, pos12 };
		line4  = new List<PiecePosition>(){ pos13, pos14, pos15, pos16 };
		line5  = new List<PiecePosition>(){ pos1,  pos5,  pos9,  pos13 };	// Verticals
		line6  = new List<PiecePosition>(){ pos2,  pos6,  pos10, pos14 };
		line7  = new List<PiecePosition>(){ pos3,  pos7,  pos11, pos15 };
		line8  = new List<PiecePosition>(){ pos4,  pos8,  pos12, pos16 };
		line9  = new List<PiecePosition>(){ pos4,  pos7,  pos10, pos13 };	// Diagonals
		line10 = new List<PiecePosition>(){ pos1,  pos6,  pos11, pos16 };
		
		//Place all lines in one collection
		winLines = new List<List<PiecePosition>>(){
			line1, line2, line3, line4, line5,
			line6, line7, line8, line9, line10
		};
	}
	
	public void checkLinesForWin(){
		
		bool   foundWin      = false;
		string winningAttrib = "";
		
		// For each line we have, we will perform
		// 8 checks on the members of the line
		// (Two checks for each of the 4 attribs)
		foreach(List<PiecePosition> line in winLines){
			
			//Check for all Dark pieces
			if(checkLineForDark(line)){
				highlightWinningLine(line);
				foundWin = true;
				winningAttrib = "Dark";
				break;
			}
			//Check for all Light pieces
			if(checkLineForLight(line)){
				highlightWinningLine(line);
				foundWin = true;
				winningAttrib = "Light";
				break;
			}
			//Check for all Round pieces
			if(checkLineForRound(line)){
				highlightWinningLine(line);
				foundWin = true;
				winningAttrib = "Round";
				break;
			}
			//Check for all Square pieces
			if(checkLineForSquare(line)){
				highlightWinningLine(line);
				foundWin = true;
				winningAttrib = "Square";
				break;
			}
			//Check for all Hollow pieces
			if(checkLineForHollow(line)){
				highlightWinningLine(line);
				foundWin = true;
				winningAttrib = "Hollow";
				break;
			}
			//Check for all Solid pieces
			if(checkLineForSolid(line)){
				highlightWinningLine(line);
				foundWin = true;
				winningAttrib = "Solid";
				break;
			}
			//Check for all Short pieces
			if(checkLineForShort(line)){
				highlightWinningLine(line);
				foundWin = true;
				winningAttrib = "Short";
				break;
			}
			//Check for all Tall pieces
			if(checkLineForTall(line)){
				highlightWinningLine(line);
				foundWin = true;
				winningAttrib = "Tall";
				break;
			}
					
		}	// END FOREACH BLOCK
		
		if(foundWin){
			Debug.Log("Game was won on a row of " + winningAttrib + " pieces!");
		}
		else {
			Debug.Log ("No win yet");
		}
		
	}
	
	bool checkLineForDark(List<PiecePosition> l){
		foreach(PiecePosition pos in l){
			if(!pos.hasDarkPiece())
				return false;
		}
		return true;
	}

	bool checkLineForLight(List<PiecePosition> l){
		foreach(PiecePosition pos in l){
			if(!pos.hasLightPiece())
				return false;
		}
		return true;
	}

	bool checkLineForRound(List<PiecePosition> l){
		foreach(PiecePosition pos in l){
			if(!pos.hasRoundPiece())
				return false;
		}
		return true;
	}
	
	bool checkLineForSquare(List<PiecePosition> l){
		foreach(PiecePosition pos in l){
			if(!pos.hasSquarePiece())
				return false;
		}
		return true;
	}
	
	bool checkLineForHollow(List<PiecePosition> l){
		foreach(PiecePosition pos in l){
			if(!pos.hasHollowPiece())
				return false;
		}
		return true;
	}
	
	bool checkLineForSolid(List<PiecePosition> l){
		foreach(PiecePosition pos in l){
			if(!pos.hasSolidPiece())
				return false;
		}
		return true;
	}
	
	bool checkLineForShort(List<PiecePosition> l){
		foreach(PiecePosition pos in l){
			if(!pos.hasShortPiece())
				return false;
		}
		return true;
	}

	bool checkLineForTall(List<PiecePosition> l){
		foreach(PiecePosition pos in l){
			if(!pos.hasTallPiece())
				return false;
		}
		return true;
	}

	void highlightWinningLine(List<PiecePosition> l){
		foreach(PiecePosition pos in l){
			pos.SendMessage("highlightPosition");
		}
	}
	
}
