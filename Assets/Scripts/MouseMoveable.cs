﻿using UnityEngine;
using System.Collections;

public class MouseMoveable : MonoBehaviour {
	
	private Vector3 originalPosition;
	private bool isMoving;
	private MouseController mouseController;

	// Use this for initialization
	void Start () {
		mouseController = GameObject.Find("MouseController").GetComponent< MouseController >();
		if(!mouseController)
			Debug.Log ("Could not find MouseController object");
		originalPosition = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	
	
	void OnMouseDown() {
		
		//Make the object the actual size
		transform.localScale = new Vector3(1F,1F,1F);
		
		//Add this to the MouseController
		if(mouseController)
			mouseController.mouseControlledObject = gameObject;
		
		Debug.Log ("Invoking MoveObject on " + this);
		InvokeRepeating("moveObject", 0, .1F);
	}
	
	void OnMouseUp() {
		
		//Stop moving the object
		Debug.Log ("Canceling MoveObject on " + this);
		CancelInvoke("moveObject");
		
		//Check to see if piece can be played in a PiecePosition
		if(mouseController.mouseHoveredPosition){
			mouseController.mouseHoveredPosition.SendMessage("playPiece", gameObject);
			
			//Prevent piece from being moved
			Destroy(GetComponent<MouseMoveable>());
		}
		else {
			//Send back to original location
			transform.position = originalPosition;
		}
		
		//Clear the mouseController's controlled object
		mouseController.mouseControlledObject = null;
	}
	
	
	void moveObject(){
		Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		Plane plane = new Plane(transform.forward, new Vector3(0,1,0));
		float distance = 0;
		if(plane.Raycast(mouseRay, out distance)){
			Vector3 newPosition = mouseRay.origin + mouseRay.direction * distance;
			//transform.position = Vector3.Lerp(transform.position, newPosition, 2*Time.deltaTime);
			transform.position = newPosition;
		}
	}
}
